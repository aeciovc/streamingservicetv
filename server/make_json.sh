############################################################
# The MIT License (MIT)
#
#Copyright (c) 2013 Evandro Lima <eslf@cesar.org.br>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.
############################################################

#!/bin/bash

json="all.json"
base_dir="svid_dir"
thumb_dir="${base_dir}/thumbnails"
open_array="{\"videos\":["
close_array="]}"
open_item="{"
close_item="}"
atrb=":"
comma=","

id="\"id\""
title="\"title\""
desc="\"description\""
addr="\"address\""
thumb="\"thumbUrl\""

files=(
`ls **/**/**/*.m3u8 | xargs -n 1`
)
it=1
len=${#files[@]}
echo "Writing json\n"
echo ${open_array} > all.json

for f in ${files[@]}
do
    echo "Item $it"
    echo -n ${open_item} >> all.json
    echo -n "${id}${atrb}\"${it}\"${comma}" >> all.json
    name=`echo $f | xargs basename | sed -e 's/.m3u8//g'`
    echo -n "${title}${atrb}\"${name}\"${comma}" >> all.json
    echo -n "${desc}${atrb}\"${name}\"${comma}" >> all.json
    echo -n "${addr}${atrb}\"/${base_dir}/${f}\"${comma}" >> all.json
    image=`echo $f | xargs basename | sed -e 's/.m3u8/.png/g'`
    echo -n "${thumb}${atrb}\"/${thumb_dir}/${image}\"" >> all.json
    if [ $it -lt $len ];then
        echo -n ${close_item}${comma} >> all.json
    else
        echo -n ${close_item} >> all.json
    fi

    it=$((it + 1))
done

echo -n ${close_array} >> all.json
