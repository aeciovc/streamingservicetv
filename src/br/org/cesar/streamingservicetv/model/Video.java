package br.org.cesar.streamingservicetv.model;

import java.io.Serializable;

import br.org.cesar.streamingservicetv.util.Util;

public class Video implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//Object
	public static String VIDEO_PARAM = "video_param";
	
	//Anymote Communication
	public static String VIDEO_URL = "url_param";
	public static String VIDEO_TITLE = "title_param";

	private String id;
	private String title;
	private String description;
	private String address;
	private String thumbUrl;
	
	public Video() {
		
	}

	
	public Video(String id, String title, String description, String address,
			String thumbUrl) {
		this.id = id;
		this.title = title;
		this.description = description;
		setAddress(Util.SERVER_URL.concat(address));
		setThumbUrl(thumbUrl);
	}


	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = Util.SERVER_URL.concat(thumbUrl);
	}

}
