package br.org.cesar.streamingservicetv.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import br.org.cesar.streamingservicetv.util.HttpUtil;
import br.org.cesar.streamingservicetv.util.Util;

public class VideoController {
	private static final String TAG = "VideoController";
	
	private static final String TAG_VIDEOS = "videos";
	private static final String TAG_ID = "id";
	private static final String TAG_TITLE = "title";
	private static final String TAG_DESCRIPTION = "description";
	private static final String TAG_ADDRESS = "address";
	private static final String TAG_THUMB_URL = "thumbUrl";
	
	/**
	 * Parse a JSON and return a list of Video object. The JSON must follow the format below.
	 *
	  {
		    "videos": [
		        {
		                "id": "1",
		                "title": "Tropa de Elite ",
		                "description": "Filme Tropa de Elite",
		                "address": "/filmes/tropa-elite.avi",
		                "thumbUrl" : "http://domain/tropa-elite.png"
		        },
		        {
		                "id": "2",
		                "title": "Homem de Ferro 2",
		                "description": "Filme Homem de Ferro 2 (HD)",
		                "address": "/filmes/home-de-ferro-2.avi",
		                "thumbUrl" : "http://domain/homem-de-ferro-2.png",
		        },
		        .
		        .
		        .
		  ]
		}
	 * @return
	 */
	public static List<Video> getVideos() {
		List<Video> videos = new ArrayList<Video>();
		String jsonString = HttpUtil.doGet(Util.SERVER_URL + Util.FILE_NAME_JSON);
		try {
			    
			JSONObject json = new JSONObject(jsonString);
			
			// Getting array of Videos
			JSONArray jsonVideos = json.getJSONArray(TAG_VIDEOS);
			     
		    // looping through All Videos
		    for(int i = 0; i < jsonVideos.length(); i++){
		        JSONObject v = jsonVideos.getJSONObject(i);
		         
		        // Storing each json item in variable
		        String id = v.getString(TAG_ID);
		        String title = v.getString(TAG_TITLE);
		        String description = v.getString(TAG_DESCRIPTION);
		        String address = v.getString(TAG_ADDRESS);
		        String thumbUrl = v.getString(TAG_THUMB_URL);
		        
		        Video video = new Video(id, title, description, address, thumbUrl);
		        videos.add(video);
		         
		    }
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage(), e);
		}
		
		return videos;
	}

}
