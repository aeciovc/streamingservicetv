package br.org.cesar.streamingservicetv;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.TextView;
import br.org.cesar.streamingservicetv.util.Util;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		
		TextView txtPresentationApp = (TextView) findViewById(R.id.txtPresentation);
		txtPresentationApp.setTypeface(Util.getTypeface(Util.TYPEFACE_LHANDW));
		
		TextView txtAppName = (TextView) findViewById(R.id.txtAppName);
		txtAppName.setTypeface(Util.getTypeface(Util.TYPEFACE_IMPACT_ASSET));
		
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this,VideosActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, 5000);
	}
	
}
