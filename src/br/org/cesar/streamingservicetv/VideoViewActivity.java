package br.org.cesar.streamingservicetv;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import br.org.cesar.streamingservicetv.model.Video;
import br.org.cesar.streamingservicetv.streaming.VideoViewTV;

public class VideoViewActivity extends Activity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_play);

		new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
            	ActionBar actionBar = getActionBar();
        		actionBar.hide();
            }
        }, 3000);
		
		Bundle bundle = getIntent().getExtras();
		
		if (bundle != null){
			
			Video video = null;
			
			if (getIntent().hasExtra(Video.VIDEO_PARAM)){
			
				video = (Video) bundle.getSerializable(Video.VIDEO_PARAM);
				
			}else  if (getIntent().hasExtra(Video.VIDEO_URL) && (getIntent().hasExtra(Video.VIDEO_TITLE))){
				video = new Video();
				video.setAddress(bundle.getString(Video.VIDEO_URL));
				video.setTitle(bundle.getString(Video.VIDEO_TITLE));
			}
			
			if (video == null){
				Toast.makeText(this, "Video not found!", Toast.LENGTH_LONG).show();
			}else{
			
				setTitle(video.getTitle());
		
				VideoViewTV videoViewTV = (VideoViewTV) findViewById(R.id.videoView);
				videoViewTV.setVideo(video);
				videoViewTV.start();
			}
	
		}
	}
	

}
