package br.org.cesar.streamingservicetv;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import br.org.cesar.streamingservicetv.model.Video;
import br.org.cesar.streamingservicetv.util.DownloadImagemUtil;
import br.org.cesar.streamingservicetv.util.Util;

public class VideoAdapter extends BaseAdapter {
	private List<Video> mVideos;
	private LayoutInflater mInflater;
	private DownloadImagemUtil mDownloader;
	
	public VideoAdapter(Context context, List<Video> videos) {
		mVideos  = videos;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mDownloader = new DownloadImagemUtil(context);
	}

	@Override
	public int getCount() {
		return mVideos.size();
	}

	@Override
	public Object getItem(int position) {
		return mVideos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
 
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			int layout = R.layout.video_item;
			convertView = mInflater.inflate(layout, null);
			
			holder.imgThumb = (ImageView) convertView.findViewById(R.id.img);
			holder.progress = (ProgressBar) convertView.findViewById(R.id.progress);
			holder.txtVideoName = (TextView) convertView.findViewById(R.id.txtVideoName);
			holder.txtVideoName.setTypeface(Util.getTypeface(Util.TYPEFACE_IMPACT_ASSET));
			convertView.setTag(holder);
		} else {
			// get from cache
			holder = (ViewHolder) convertView.getTag();
		}
		//holder.imgThumb.setImageBitmap(null);
		Video video = mVideos.get(position);
		mDownloader.download(video.getThumbUrl(), holder.imgThumb, holder.progress);
		holder.txtVideoName.setText(video.getTitle());
		return convertView;
	}
	
	// "ViewHolder" DesignPattern for Android
	static class ViewHolder {
		ImageView imgThumb;
		ProgressBar progress;
		TextView txtVideoName;
	}

}
