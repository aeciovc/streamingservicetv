package br.org.cesar.streamingservicetv;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import br.org.cesar.streamingservicetv.model.Video;
import br.org.cesar.streamingservicetv.model.VideoController;

public class VideosActivity extends Activity {

	private GridView gridView;
	private VideoAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		gridView = (GridView) findViewById(R.id.gridview);
		gridView.requestFocus();
		
		new GetVideosTask().execute();
	}

	class GetVideosTask extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog dialog = new ProgressDialog(VideosActivity.this);
		List<Video> videos;

		protected void onPreExecute() {
			dialog.setMessage("Please wait...");
			dialog.setIndeterminate(true);
			dialog.show();
		}

		protected void onPostExecute(Void unused) {
			
			gridView.setAdapter(adapter);
			gridView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
					
					Intent intent = new Intent(VideosActivity.this, VideoViewActivity.class);
					intent.putExtra(Video.VIDEO_PARAM, videos.get(position));
					startActivity(intent);
				}
			});
			
			dialog.dismiss();
		}

		@Override
		protected Void doInBackground(Void... params) {
			
			videos = VideoController.getVideos();
			// http://crk_br_am_ios-i.akamaihd.net/i/1/w/z6/jsvlf_H264_IOS_,100BR,200BR,350BR,500BR,650BR,800BR,900BR,.mp4.csmil/master.m3u8?__b__=400&__a__=off
			
			adapter = new VideoAdapter(VideosActivity.this, videos);
			return null;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.activity_main, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		startActivity(new Intent(VideosActivity.this, SettingsActivity.class));
		
		return super.onMenuItemSelected(featureId, item);
	}

}
