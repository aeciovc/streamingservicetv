package br.org.cesar.streamingservicetv.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
	
	public static String KEY_APP = "br.org.cesar.streamingservicetv";
	public static String KEY_ADDRESS = "address_server";
	
	private SharedPreferences mPrefs;

	public Preferences(Context context) {
		mPrefs = context.getSharedPreferences(KEY_APP, Context.MODE_PRIVATE);
	}
	
	public void update(String keyName, String value){
		
		SharedPreferences.Editor editor = mPrefs.edit();
		editor.putString(keyName, value);
		editor.commit();
	}
	
	public String getValue(String key, String defValue){
		return mPrefs.getString(key, defValue);
	}

}
