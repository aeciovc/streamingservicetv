package br.org.cesar.streamingservicetv.util;

import android.app.Application;
import android.content.Context;


public class UtilContext extends Application {
	
	private static Context context;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		context = getApplicationContext();
	}
	
	public static Context getContextApp() {
		return context;
	}
}
