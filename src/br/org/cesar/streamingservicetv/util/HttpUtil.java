package br.org.cesar.streamingservicetv.util;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class HttpUtil {
	
	private static final String TAG = "HttpUtil";
	
	public static String doGet(String url) {
		return doGet(url, HTTP.UTF_8);
	}
	
	public static String doGet(String url, String charset) {
		String responseString = "";
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		try {
		   HttpResponse response = client.execute(request);
		   responseString = EntityUtils.toString(response.getEntity(), charset);
		}  catch (IOException e) {
			Log.e(TAG, e.getMessage(), e);
		}

		return responseString;
	}

}
